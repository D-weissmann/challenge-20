## Challenge 20

# Overview
    This was an attempt to try and figure out the charities that are more likely to succeed so that AlphabetSoup can more effectively donate. 

#Results
Data Preprocessing
- The only target was whether or not the charity was succesful. 

- The main variables were the Application Type, The Income Amount, the Use Case and the Ask Amount

- I should remove the Classification, the organization, and the status from this 

Compiling Training and Evaluating the model

- I tried several number of neurons and layers, I ended up with 3 layers of 10,7,5 

- I was not able to achieve the accuracy

-I tried several different types of models, using relu, leaky relu, sigmoid functions, I tried  2 and 3 hidden layers, with several different numbers of neurons

#Summary and recomendations
unfortunately I was unable to get over a 55% accuracy. I'm sure there is another type of study that would work better for this data set unfortunately I clearly don't know what it is. 
